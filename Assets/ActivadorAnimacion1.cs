﻿using UnityEngine;
using System.Collections;

public class ActivadorAnimacion1 : MonoBehaviour {

	private EventoDeCruz actv;

	private Animator animar;

	// Use this for initialization
	void Start () {
	
		actv = FindObjectOfType<EventoDeCruz> ();

		animar = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (actv.activador) {

			animar.enabled = true;
		}

	}
}
