﻿using UnityEngine;
using System.Collections;

public class ActivadorAnimacion2 : MonoBehaviour {

	private EventoDeCruz2 actv;

	private Animator animar;

	// Use this for initialization
	void Start () {

		actv = FindObjectOfType<EventoDeCruz2> ();

		animar = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {

		if (actv.activador2) {

			animar.enabled = true;
		}

	}
}
