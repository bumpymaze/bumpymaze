﻿using UnityEngine;
using System.Collections;

public class ActivadorAnimacion3 : MonoBehaviour {


	private EventoDeCruz3 actv;

	private Animator animar;

	// Use this for initialization
	void Start () {

		actv = FindObjectOfType<EventoDeCruz3> ();

		animar = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {

		if (actv.activador3) {

			animar.enabled = true;
		}

	}
}
