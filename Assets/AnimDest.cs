﻿using UnityEngine;
using System.Collections;

public class AnimDest : MonoBehaviour {

	private Animator animchiz;
	private Collider colider;

	// Use this for initialization
	void Start () {
		animchiz = gameObject.GetComponent<Animator> ();
		colider = gameObject.GetComponent<Collider> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider collider)
	{


		animchiz.enabled = true;
		colider.enabled = false;
		
		
	}

}
