﻿using UnityEngine;
using System.Collections;

public class Bonuses : MonoBehaviour {


	private Transform player;
	public GameObject shield;
	// Use this for initialization
	void Start () {
		player =  GameObject.Find ("Player").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
	

		if (Config.BonusElectricShield) {

			GameObject BnusShield = Instantiate (shield, player.position, player.rotation)as GameObject;
			Config.BonusElectricShield = false;

		}

	}
}
