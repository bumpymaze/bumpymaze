﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour {
	public Material mate;
	private Animator anim;
	private Renderer render;
	public int Existencia = 4;
	public GameObject destruccion;

	// Use this for initialization
	void Start () {
	
		//render = gameObject.GetComponent<MeshRenderer> ();
		render = GetComponent<Renderer>();
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Existencia == 0) {

			Debug.Log ("murieron todos!!");

			anim.Rebind();
			anim.enabled = false;
			//render.sharedMaterial = mate;

		}

	}

	void OnTriggerEnter(Collider Otro) {

		if (Existencia == 0) {
			Debug.Log ("toca el boss?");
			if (Otro.tag == "Catapulobject") {
				
				GameObject anim_destruccion = Instantiate (destruccion, transform.position, transform.rotation)as GameObject;
		
				Destroy (gameObject);
				Debug.Log ("Destrulle?");
			}
		}


}
}
