﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cargadeniveles : MonoBehaviour {
	private Button habilvl2;
	private Image image2;

	private Button habilvl3;
	private Image image3;

	private Button habilvl4;
	private Image image4;

	private Button habilvl5;
	private Image image5;

	private Button habilvl6;
	private Image image6;

	private Button habilvl7;
	private Image image7;

	private Button habilvl8;
	private Image image8;

	private Button habilvl9;
	private Image image9;

	private Button habilvl10;
	private Image image10;



	public Text scorelvl1;
	public Text scorelvl2;
	public Text scorelvl3;
	public Text scorelvl4;
	public Text scorelvl5;
	public Text scorelvl6;
	public Text scorelvl7;
	public Text scorelvl8;
	public Text scorelvl9;
	public Text scorelvl10;

	/// T//////////////////	/// WORDL 2  //// 
	/// T/////	///




	// Use this for initialization
	void Start () {
		habilvl2 =  GameObject.Find ("Lvl2i").GetComponent<Button> ();
		image2 = GameObject.Find ("Lvl2i").GetComponent<Image> ();

		habilvl3 =  GameObject.Find ("Lvl3i").GetComponent<Button> ();
		image3 = GameObject.Find ("Lvl3i").GetComponent<Image> ();

		habilvl4 =  GameObject.Find ("Lvl4i").GetComponent<Button> ();
		image4 = GameObject.Find ("Lvl4i").GetComponent<Image> ();

		habilvl5 =  GameObject.Find ("Lvl5i").GetComponent<Button> ();
		image5 = GameObject.Find ("Lvl5i").GetComponent<Image> ();

		habilvl6 =  GameObject.Find ("Lvl6i").GetComponent<Button> ();
		image6 = GameObject.Find ("Lvl6i").GetComponent<Image> ();

		habilvl7 =  GameObject.Find ("Lvl7i").GetComponent<Button> ();
		image7 = GameObject.Find ("Lvl7i").GetComponent<Image> ();

		habilvl8 =  GameObject.Find ("Lvl8").GetComponent<Button> ();
		image8 = GameObject.Find ("Lvl8").GetComponent<Image> ();

		habilvl9 =  GameObject.Find ("Lvl9").GetComponent<Button> ();
		image9 = GameObject.Find ("Lvl9").GetComponent<Image> ();

		habilvl10 =  GameObject.Find ("Lvl10").GetComponent<Button> ();
		image10 = GameObject.Find ("Lvl10").GetComponent<Image> ();

		///////////// PARA PRUEBAS LUEGO BORRAR ////////////////////////
		 
		habilvl2.enabled = true;
		habilvl3.enabled = true;
		habilvl4.enabled = true;
		habilvl5.enabled = true;
		habilvl6.enabled = true;
		habilvl7.enabled = true;
		habilvl8.enabled = true;
		habilvl9.enabled = true;
		habilvl10.enabled = true;




		 
		///////////////////BORRAR/////////////////////////////////////// 

	}
	
	// Update is called once per frame
	void Update () {



	if (Estadolevels.progresolvl >= 2) {

			image2.color = new Color32(201,84,247,255); 
			habilvl2.enabled = true;
			//Debug.Log ("HABILITO LVL2");
			scorelvl1.text = "Record: " + (Estadolevels.besttime[1]/100).ToString ("0#") + ":" + (Estadolevels.besttime[1] - ((Estadolevels.besttime[1]/100)*100)).ToString ("0#"); 
		}
       

		if (Estadolevels.progresolvl >= 3) {
			
			image3.color = new Color32(245,34,44,255); 
			habilvl3.enabled = true;
			//Debug.Log ("HABILITO LVL3");
			scorelvl2.text = "Record: " + (Estadolevels.besttime[2]/100).ToString ("0#") + ":" + (Estadolevels.besttime[2] - ((Estadolevels.besttime[2]/100)*100)).ToString ("0#"); 
		}

		if (Estadolevels.progresolvl >= 4) {
			
			image4.color = new Color32(104,230,96,255); 
			habilvl4.enabled = true;
			//Debug.Log ("HABILITO LVL4");
			scorelvl3.text = "Record: " + (Estadolevels.besttime[3]/100).ToString ("0#") + ":" + (Estadolevels.besttime[3] - ((Estadolevels.besttime[3]/100)*100)).ToString ("0#"); 
		}


		if (Estadolevels.progresolvl >= 5) {
			
			image5.color = new Color32(249,252,58,255); 
			habilvl5.enabled = true;
			//Debug.Log ("HABILITO LVL5");
			scorelvl4.text = "Record: " + (Estadolevels.besttime[4]/100).ToString ("0#") + ":" + (Estadolevels.besttime[4] - ((Estadolevels.besttime[4]/100)*100)).ToString ("0#"); 
		}

		if (Estadolevels.progresolvl >= 6) {
			
			image6.color = new Color32(191,128,69,255); 
			habilvl6.enabled = true;
			//Debug.Log ("HABILITO LVL6");
			scorelvl5.text = "Record: " + (Estadolevels.besttime[5]/100).ToString ("0#") + ":" + (Estadolevels.besttime[5] - ((Estadolevels.besttime[5]/100)*100)).ToString ("0#");  
		}

		if (Estadolevels.progresolvl >= 7) {
			
			image7.color = new Color32(42,227,255,255); 
			habilvl7.enabled = true;
			//Debug.Log ("HABILITO LVL7");
			scorelvl6.text = "Record: " + (Estadolevels.besttime[6]/100).ToString ("0#") + ":" + (Estadolevels.besttime[6] - ((Estadolevels.besttime[6]/100)*100)).ToString ("0#"); 
		}

		if (Estadolevels.progresolvl >= 8) {

			image8.color = new Color32(233,10,199,255); 
			habilvl8.enabled = true;
			//Debug.Log ("HABILITO LVL8");
			scorelvl7.text = "Record: " + (Estadolevels.besttime[7]/100).ToString ("0#") + ":" + (Estadolevels.besttime[7] - ((Estadolevels.besttime[7]/100)*100)).ToString ("0#");  
		}

		if (Estadolevels.progresolvl >= 9) {

			image9.color = new Color32(17,92,7,255); 
			habilvl9.enabled = true;
			//Debug.Log ("HABILITO LVL9");
			scorelvl8.text = "Record: " + (Estadolevels.besttime[8]/100).ToString ("0#") + ":" + (Estadolevels.besttime[8] - ((Estadolevels.besttime[8]/100)*100)).ToString ("0#");  
		}

		if (Estadolevels.progresolvl >= 10) {

			image10.color = new Color32(163,19,214,255); 
			habilvl10.enabled = true;
			//Debug.Log ("HABILITO LVL10");
			scorelvl9.text = "Record: " + (Estadolevels.besttime[9]/100).ToString ("0#") + ":" + (Estadolevels.besttime[9] - ((Estadolevels.besttime[9]/100)*100)).ToString ("0#"); 
		}



	}
}
