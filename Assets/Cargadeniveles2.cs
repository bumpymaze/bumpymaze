﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cargadeniveles2 : MonoBehaviour {

	private Button habilvl11;
	private Image image11;

	private Button habilvl12;
	private Image image12;

	private Button habilvl13;
	private Image image13;

	private Button habilvl14;
	private Image image14;

	private Button habilvl15;
	private Image image15;

	private Button habilvl16;
	private Image image16;

	private Button habilvl17;
	private Image image17;

	private Button habilvl18;
	private Image image18;

	private Button habilvl19;
	private Image image19;

	private Button habilvl20;
	private Image image20;



	public Text scorelvl11;
	public Text scorelvl12;
	public Text scorelvl13;
	public Text scorelvl14;
	public Text scorelvl15;
	public Text scorelvl16;
	public Text scorelvl17;
	public Text scorelvl18;
	public Text scorelvl19;
	public Text scorelvl20;

	/// T//////////////////	/// WORDL 2  //// 
	/// T/////	///




	// Use this for initialization
	void Start () {

		habilvl11 =  GameObject.Find ("Lvl12i").GetComponent<Button> ();
		image11 = GameObject.Find ("Lvl12i").GetComponent<Image> ();

		habilvl12 =  GameObject.Find ("Lvl12i").GetComponent<Button> ();
		image12 = GameObject.Find ("Lvl12i").GetComponent<Image> ();

		habilvl13 =  GameObject.Find ("Lvl13i").GetComponent<Button> ();
		image13 = GameObject.Find ("Lvl13i").GetComponent<Image> ();

		habilvl14 =  GameObject.Find ("Lvl14i").GetComponent<Button> ();
		image14 = GameObject.Find ("Lvl14i").GetComponent<Image> ();

		habilvl15 =  GameObject.Find ("Lvl15i").GetComponent<Button> ();
		image15 = GameObject.Find ("Lvl15i").GetComponent<Image> ();

		habilvl16 =  GameObject.Find ("Lvl16i").GetComponent<Button> ();
		image16 = GameObject.Find ("Lvl16i").GetComponent<Image> ();

		habilvl17 =  GameObject.Find ("Lvl17i").GetComponent<Button> ();
		image17 = GameObject.Find ("Lvl17i").GetComponent<Image> ();

		habilvl18 =  GameObject.Find ("Lvl18").GetComponent<Button> ();
		image18 = GameObject.Find ("Lvl18").GetComponent<Image> ();

		habilvl19 =  GameObject.Find ("Lvl19").GetComponent<Button> ();
		image19 = GameObject.Find ("Lvl19").GetComponent<Image> ();

		habilvl20 =  GameObject.Find ("Lvl20").GetComponent<Button> ();
		image20 = GameObject.Find ("Lvl20").GetComponent<Image> ();

		///////////// PARA PRUEBAS LUEGO BORRAR ////////////////////////

		habilvl11.enabled = true;
		habilvl12.enabled = true;
		habilvl13.enabled = true;
		habilvl14.enabled = true;
		habilvl15.enabled = true;
		habilvl16.enabled = true;
		habilvl17.enabled = true;
		habilvl18.enabled = true;
		habilvl19.enabled = true;
		habilvl20.enabled = true;





		///////////////////BORRAR/////////////////////////////////////// 

	}

	// Update is called once per frame
	void Update () {

		if (Estadolevels.progresolvl >= 11) {

			image11.color = new Color32(201,84,247,255); 
			habilvl11.enabled = true;
			//Debug.Log ("HABILITO LVL2");
			 
		}



		if (Estadolevels.progresolvl >= 12) {

			image12.color = new Color32(201,84,247,255); 
			habilvl12.enabled = true;
			//Debug.Log ("HABILITO LVL2");
			scorelvl11.text = "Record: " + (Estadolevels.besttime[11]/100).ToString ("0#") + ":" + (Estadolevels.besttime[11] - ((Estadolevels.besttime[11]/100)*100)).ToString ("0#"); 
		}


		if (Estadolevels.progresolvl >= 13) {

			image13.color = new Color32(245,34,44,255); 
			habilvl13.enabled = true;
			//Debug.Log ("HABILITO LVL3");
			scorelvl12.text = "Record: " + (Estadolevels.besttime[12]/100).ToString ("0#") + ":" + (Estadolevels.besttime[12] - ((Estadolevels.besttime[12]/100)*100)).ToString ("0#"); 
		}

		if (Estadolevels.progresolvl >= 14) {

			image14.color = new Color32(104,230,96,255); 
			habilvl14.enabled = true;
			//Debug.Log ("HABILITO LVL4");
			scorelvl13.text = "Record: " + (Estadolevels.besttime[13]/100).ToString ("0#") + ":" + (Estadolevels.besttime[13] - ((Estadolevels.besttime[13]/100)*100)).ToString ("0#"); 
		}


		if (Estadolevels.progresolvl >= 15) {

			image15.color = new Color32(249,252,58,255); 
			habilvl15.enabled = true;
			//Debug.Log ("HABILITO LVL5");
			scorelvl14.text = "Record: " + (Estadolevels.besttime[14]/100).ToString ("0#") + ":" + (Estadolevels.besttime[14] - ((Estadolevels.besttime[14]/100)*100)).ToString ("0#"); 
		}

		if (Estadolevels.progresolvl >= 16) {

			image16.color = new Color32(191,128,69,255); 
			habilvl16.enabled = true;
			//Debug.Log ("HABILITO LVL6");
			scorelvl15.text = "Record: " + (Estadolevels.besttime[15]/100).ToString ("0#") + ":" + (Estadolevels.besttime[15] - ((Estadolevels.besttime[15]/100)*100)).ToString ("0#");  
		}

		if (Estadolevels.progresolvl >= 17) {

			image17.color = new Color32(42,227,255,255); 
			habilvl17.enabled = true;
			//Debug.Log ("HABILITO LVL7");
			scorelvl16.text = "Record: " + (Estadolevels.besttime[16]/100).ToString ("0#") + ":" + (Estadolevels.besttime[16] - ((Estadolevels.besttime[16]/100)*100)).ToString ("0#"); 
		}

		if (Estadolevels.progresolvl >= 18) {

			image18.color = new Color32(42,227,255,255); 
			habilvl18.enabled = true;
			//Debug.Log ("HABILITO LVL8");
			scorelvl17.text = "Record: " + (Estadolevels.besttime[17]/100).ToString ("0#") + ":" + (Estadolevels.besttime[17] - ((Estadolevels.besttime[17]/100)*100)).ToString ("0#");  
		}

		if (Estadolevels.progresolvl >= 19) {

			image19.color = new Color32(42,227,255,255); 
			habilvl19.enabled = true;
			//Debug.Log ("HABILITO LVL9");
			scorelvl18.text = "Record: " + (Estadolevels.besttime[18]/100).ToString ("0#") + ":" + (Estadolevels.besttime[18] - ((Estadolevels.besttime[18]/100)*100)).ToString ("0#");  
		}

		if (Estadolevels.progresolvl >= 20) {

			image20.color = new Color32(42,227,255,255); 
			habilvl20.enabled = true;
			//Debug.Log ("HABILITO LVL10");
			scorelvl19.text = "Record: " + (Estadolevels.besttime[19]/100).ToString ("0#") + ":" + (Estadolevels.besttime[19] - ((Estadolevels.besttime[19]/100)*100)).ToString ("0#"); 
		}



	}
}
