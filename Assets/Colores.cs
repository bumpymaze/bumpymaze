﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Colores : MonoBehaviour {
	

	private Image Cara;

	// Use this for initialization
	void Start () {
	
		Cara = GameObject.Find ("Cara").GetComponent<Image> ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BotonNegro()
	{

		Cara.color = new Color32(0,0,0,255); 
	}

	public void BotonGris()
	{

		Cara.color = new Color32(118,113,113,255); 
	}

	public void BotonBlanco()
	{

		Cara.color = new Color32(255,255,255,255); 
	}

	public void BotonBordo()
	{

		Cara.color = new Color32(92,4,4,255); 
	}

	public void BotonRojo()
	{

		Cara.color = new Color32(201,13,13,255); 
	}

	public void BotonRojoclaro()
	{

		Cara.color = new Color32(255,0,111,255); 
	}

	public void BotonAzulOsucuro()
	{

		Cara.color = new Color32(9,2,219,255); 
	}

	public void BotonAzulClaro()
	{

		Cara.color = new Color32(58,139,219,255); 
	}

	public void BotonCeleste()
	{

		Cara.color = new Color32(12,195,240,255); 
	}

	public void BotonVerdeOscuro()
	{

		Cara.color = new Color32(40,77,10,255); 
	}

	public void Botonverdeclaro()
	{

		Cara.color = new Color32(25,221,18,255); 
	}

	public void BotonTurquesa()
	{

		Cara.color = new Color32(11,246,178,255); 
	}

	public void BotonAmarilloOscuro()
	{

		Cara.color = new Color32(253,214,0,255); 
	}

	public void BotonAmarillo()
	{

		Cara.color = new Color32(255,255,40,255); 
	}

	public void BotonAmarilloClaro()
	{

		Cara.color = new Color32(255,255,143,255); 
	}

	public void BotonMarron()
	{

		Cara.color = new Color32(124,72,6,255); 
	}

	public void BotonNaranja()
	{

		Cara.color = new Color32(255,153,0,255); 
	}

	public void BotonNaranjaClaro()
	{

		Cara.color = new Color32(255,200,101,255); 
	}

	public void BotonVioleta()
	{

		Cara.color = new Color32(81,44,120,255); 
	}

	public void BotonVioletaclaro()
	{

		Cara.color = new Color32(176,18,193,255); 
	}

	public void BotonRosa()
	{

		Cara.color = new Color32(255,105,226,255); 
	}

}
