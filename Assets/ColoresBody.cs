﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColoresBody : MonoBehaviour {


	public Material body;
	private Renderer render;



	// Use this for initialization
	void Start () {
		//body = GameObject.Find ("Body1").GetComponent<Material> ();
		//render = GameObject.Find ("Body1").GetComponent<Renderer> ();


	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void BotonNegroBody()
	{

		body.color = new Color (0,0,0,255); 

		Config.ColorPlayer = 1;

	}

	public void BotonGrisBody()
	{

		body.color = new Color32(118,113,113,255); 
		Config.ColorPlayer = 2;
	}

	public void BotonBlancoBody()
	{

		body.color = new Color32(255,255,255,255); 
		Config.ColorPlayer = 3;
	}

	public void BotonBordoBody()
	{

		body.color = new Color32(92,4,4,255); 
		Config.ColorPlayer = 4;
	}

	public void BotonRojoBody()
	{

		body.color = new Color32(201,13,13,255); 
		Config.ColorPlayer = 5;
	}

	public void BotonRojoclaroBody()
	{

		body.color = new Color32(255,0,111,255); 
		Config.ColorPlayer = 6;
	}

	public void BotonAzulOsucuroBody()
	{

		body.color = new Color32(9,2,219,255); 
		Config.ColorPlayer = 7;
	}

	public void BotonAzulClaroBody()
	{

		body.color = new Color32(58,139,219,255);
		Config.ColorPlayer = 8;
	}

	public void BotonCelesteBody()
	{

		body.color = new Color32(12,195,240,255); 
		Config.ColorPlayer = 9;
	}

	public void BotonVerdeOscuroBody()
	{

		body.color = new Color32(40,77,10,255); 
		Config.ColorPlayer = 10;
	}

	public void BotonverdeclaroBody()
	{

		body.color = new Color32(25,221,18,255);
		Config.ColorPlayer = 11;
	}

	public void BotonTurquesaBody()
	{

		body.color = new Color32(11,246,178,255); 
		Config.ColorPlayer = 12;
	}

	public void BotonAmarilloOscuroBody()
	{

		body.color = new Color32(253,214,0,255); 
		Config.ColorPlayer = 13;
	}

	public void BotonAmarilloBody()
	{

		body.color = new Color32(255,255,40,255); 
		Config.ColorPlayer = 14;
	}

	public void BotonAmarilloClaroBody()
	{

		body.color = new Color32(255,255,143,255); 
		Config.ColorPlayer = 15;
	}

	public void BotonMarronBody()
	{

		body.color = new Color32(124,72,6,255); 
		Config.ColorPlayer = 16;
	}

	public void BotonNaranjaBody()
	{

		body.color = new Color32(255,153,0,255);
		Config.ColorPlayer = 17;
	}

	public void BotonNaranjaClaro()
	{

		body.color = new Color32(255,200,101,255);
		Config.ColorPlayer = 18;
	}

	public void BotonVioletaBody()
	{

		body.color = new Color32(81,44,120,255); 
		Config.ColorPlayer = 19;
	}

	public void BotonVioletaclaroBody()
	{

		body.color = new Color32(176,18,193,255); 
		Config.ColorPlayer = 20;
	}

	public void BotonRosaBody()
	{

		body.color = new Color32(255,105,226,255); 
		Config.ColorPlayer = 21;
	}

}
