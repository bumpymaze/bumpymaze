﻿using UnityEngine;
using System.Collections;

public class Compras : MonoBehaviour {

	public GameObject CarteldeAdvertencia;
	public GameObject CartelDeSeguroDeCompra;
	bool compro = false;
	int articuloDeCompra = 0;

	// Use this for initialization
	void Start () {
		//CarteldeAdvertencia =  GameObject.Find ("CartelAdvert");
		CarteldeAdvertencia.SetActive(false);
		CartelDeSeguroDeCompra.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void ComprarSegunCorresponda()
	{

		switch (articuloDeCompra) {

		case 1:
			{

				Estadolevels.CoinsGanadas -= 15;
				Config.bonus5 += 1; 
				articuloDeCompra = 0;
				break;
			}

		case 2:
			{
				Estadolevels.CoinsGanadas -= 25;
				Config.bonus10 += 1; 
				articuloDeCompra = 0;
				break;
			}

		case 3:
			{

				Estadolevels.CoinsGanadas -= 70;
				Config.BonusElectricShield = true;
				articuloDeCompra = 0;
				break;
			}
		}
	}

/*

	public void ComprarBonus10()
	{


		if (Estadolevels.Coins >= 25) {
			
			Estadolevels.CoinsGanadas -= 25;
			Config.bonus10 += 1; 
		} else {

			CarteldeAdvertencia.SetActive(true);
		}
	}


	public void ComprarBonusElectricShield()
	{


		if (Estadolevels.Coins >= 70) {

			Estadolevels.CoinsGanadas -= 70;
		} else {

			CarteldeAdvertencia.SetActive(true);
		}
	}


	public void ComprarColoresCara()
	{


		if (Estadolevels.Coins >= 200) {

			Estadolevels.CoinsGanadas -= 200;
		} else {

			CarteldeAdvertencia.SetActive(true);
		}
	}

	public void ComprarColoresBody()
	{
		if (Estadolevels.Coins >= 150) {

			Estadolevels.CoinsGanadas -= 150;
		} else {

			CarteldeAdvertencia.SetActive(true);
		}

	}

	public void ComprarSkin()
	{


		if (Estadolevels.Coins >= 300) {

			Estadolevels.CoinsGanadas -= 300;
		} else {

			CarteldeAdvertencia.SetActive(true);
		}
	}

	public void DeseoComprarlo()
	{
		compro = true;
	}

*/
	public void IntentoComprarBonus5()
	{

		if (Estadolevels.Coins >= 15) {
			CartelDeSeguroDeCompra.SetActive (true);
			articuloDeCompra = 1; 
		}else
		{
			CarteldeAdvertencia.SetActive(true);

		}
	}

	public void IntentoComprarBonus10()
	{

		if (Estadolevels.Coins >= 25) {
			CartelDeSeguroDeCompra.SetActive (true);
			articuloDeCompra = 2;
		}else
		{
			CarteldeAdvertencia.SetActive(true);
		}
	}

	public void IntentoComprarElectricShield()
	{

		if (Estadolevels.Coins >= 70) {
			CartelDeSeguroDeCompra.SetActive (true);
			articuloDeCompra = 3;
		}else
		{
			CarteldeAdvertencia.SetActive(true);
		}
	}

	public void IntentoComprarColoresCara()
	{

		if (Estadolevels.Coins >= 200) {
			CartelDeSeguroDeCompra.SetActive (true);
			articuloDeCompra = 4;
		}else
		{
			CarteldeAdvertencia.SetActive(true);
		}
	}

	public void IntentoComprarColoresBody()
	{

		if (Estadolevels.Coins >= 150) {
			CartelDeSeguroDeCompra.SetActive (true);
			articuloDeCompra = 5;
		}else
		{
			CarteldeAdvertencia.SetActive(true);
		}
	}

	public void IntentoComprarSkins()
	{

		if (Estadolevels.Coins >= 300) {
			CartelDeSeguroDeCompra.SetActive (true);
			articuloDeCompra = 6;
		}else
		{
			CarteldeAdvertencia.SetActive(true);
		}
	}
}
