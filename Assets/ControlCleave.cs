﻿using UnityEngine;
using System.Collections;

public class ControlCleave : MonoBehaviour {
	private ParticleSystem Control;
	private EventoDeCleave actv;
	public  bool controlador = false;

	// Use this for initialization
	void Start () {
		Control =  gameObject.GetComponent<ParticleSystem> ();
		actv = FindObjectOfType<EventoDeCleave> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (actv.activador) {
			Debug.Log ("Toma el activador?");
			Reinicio ();
			actv.segundo =1;
			actv.activador = false;
		} 	
	
	}

	public void Reinicio()
	{
		Control.Play ();
	}
}
