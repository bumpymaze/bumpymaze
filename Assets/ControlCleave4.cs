﻿using UnityEngine;
using System.Collections;

public class ControlCleave4 : MonoBehaviour {

	private ParticleSystem Control;
	private EventoDeCleave4 actv;
	public  bool controlador = false;

	// Use this for initialization
	void Start () {
		Control =  gameObject.GetComponent<ParticleSystem> ();
		actv = FindObjectOfType<EventoDeCleave4> ();
	}

	// Update is called once per frame
	void Update () {

		if (actv.activador) {
			
			Reinicio ();
			actv.segundo =1;
			actv.activador = false;
		} 	

	}

	public void Reinicio()
	{
		Control.Play ();
	}
}