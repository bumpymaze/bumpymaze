﻿using UnityEngine;
using System.Collections;

public class Destruccion_Plataforma : MonoBehaviour {


	float choques =0;
	public Material mate;
	public GameObject rotura;

	private Renderer render;
	// Use this for initialization
	void Start () {
		render = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}




	void OnTriggerEnter(Collider Otro) {
		if (Otro.tag == "Player") {

			if (choques == 0) {
				render.sharedMaterial = mate;

				Invoke ("Esperar",(float)0.5);
			}


			if (choques == 1) {
				
				Invoke ("Destruir",(float)0.2);
			}



		}
	}

	public void Esperar(){
		choques = 1;

	}

	public void Destruir(){
		Destroy (gameObject);
		GameObject animacionRotura = Instantiate (rotura, transform.position, transform.rotation)as GameObject;

	}

}
