﻿using UnityEngine;
using System.Collections;

public class EmpezarMovimiento : MonoBehaviour {
	
	private EventoDePlataforma actv;
	private Animator animar;
	// Use this for initialization
	void Start () {
		actv = FindObjectOfType<EventoDePlataforma> ();
		animar = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (actv.activador) {

			animar.enabled = true;
		}
	}
}
