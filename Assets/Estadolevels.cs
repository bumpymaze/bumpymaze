﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class Estadolevels : MonoBehaviour {
	public static Estadolevels estadolvl;


	public static int accelvl = 1;
	public static int progresolvl = 0;
	public static int[] besttime = new int[100];// AGREGADO
	public static int[] timelvl= new int[100];// AGREGADO
	public static int Coins;                //total coins
	public static int CoinsGanadas;		    // coins que gano
	private string rutaArchivo;
	public static int x=0;
	public static int[] scorelvl = new int[100];      		// score del lvl que jugo
	public static int[] bestcorelvl = new int[100];			// mejor score del lvl que jugo
	public static int totalScore;			// suma de todos los score

	int i;





	void Awake(){


		rutaArchivo = Application.persistentDataPath + "/datosBM23.dat";
		if (estadolvl == null) {
			estadolvl = this;
			DontDestroyOnLoad (gameObject);  //NO DESTRUIR 

			PlayGamesPlatform.DebugLogEnabled = true;
			PlayGamesPlatform.Activate ();  // Activa el plugin de google play

			if (Social.localUser.authenticated) {
				Debug.Log("Ya autenticado");
			} else {
				Social.localUser.Authenticate ((bool success) => {
				});
			}

		} else if (estadolvl != this) {
			Destroy(gameObject);
		}


	}


	// Use this for initialization
	void Start () {
		
		((PlayGamesPlatform)Social.Active).Authenticate ((bool success) => {
			if(success)
			{
				Debug.Log("log in succes");

			}else Debug.Log("LOG in failed");

		}, false); // si ya habia iniciado secion antes no se le pide otra ves, se inicia automaticamente	

		Cargar (); // Carga los datos previamente guardados


	}
	
	// Update is called once per frame
	void Update () {


		if (accelvl > progresolvl) {
			progresolvl = accelvl;
			Guardar();

				
		}
		if (timelvl[x] > besttime[x]) {
			besttime[x] = timelvl[x];
			Guardar ();

		}


		if (Coins != CoinsGanadas) {
			Coins = CoinsGanadas;   
			Guardar ();
		}

		if (scorelvl[x] > bestcorelvl[x]) {
			bestcorelvl[x] = scorelvl[x];
			Guardar ();

		}
	}

	void Guardar()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		BinaryFormatter bf2 = new BinaryFormatter ();
		BinaryFormatter bf3 = new BinaryFormatter ();
		FileStream file = File.Create(rutaArchivo);

	
		DatosAGuardar datos = new DatosAGuardar (progresolvl);
		datos.progresolvl = progresolvl;

		DatosAGuardar tiempo = new DatosAGuardar (besttime);
		for (i = 0; i < 99; i++) {
			tiempo.besttimeguard [i] = besttime [i];
		}

		DatosdeMonedas Monedas = new DatosdeMonedas (Coins);
		Monedas.Coins = Coins;

		DatosScore Score = new DatosScore (totalScore);				// guarda los datos del total score
		Score.totalScore = totalScore;

		DatosScore Scoreforlvl = new DatosScore (bestcorelvl);
		for (i = 0; i < 99; i++) {
			Scoreforlvl.bestscoreguard [i] = bestcorelvl [i];
		}



		bf.Serialize (file, datos);
		bf.Serialize (file, tiempo);
		bf2.Serialize (file, Monedas);
		bf3.Serialize (file, Score);
		bf3.Serialize (file, Scoreforlvl);

		file.Close ();



	}

	void Cargar()
	{
		if (File.Exists (rutaArchivo)) { //SI el archivo existe (le doy la ruta)
			
			BinaryFormatter bf = new BinaryFormatter (); //comando de binary formaterr
			BinaryFormatter bf2 = new BinaryFormatter ();
			BinaryFormatter bf3 = new BinaryFormatter ();
			FileStream file = File.Open (rutaArchivo, FileMode.Open);	//Abro el archivo para escritura




			DatosAGuardar datos = (DatosAGuardar)bf.Deserialize (file);
			progresolvl = datos.progresolvl;

			DatosAGuardar tiempo = (DatosAGuardar)bf.Deserialize (file);
			for (i = 0; i < 99; i++) {
				besttime [i] = tiempo.besttimeguard [i];
				}

			DatosdeMonedas Monedas = (DatosdeMonedas)bf2.Deserialize (file);
			Coins = Monedas.Coins;


			DatosScore Score = (DatosScore)bf3.Deserialize(file);				//levanto el score
			totalScore = Score.totalScore;

			DatosScore Scoreforlvl = (DatosScore)bf3.Deserialize(file);
			for (i = 0; i < 99; i++) {
				bestcorelvl [i] = Scoreforlvl.bestscoreguard [i];
			}


			CoinsGanadas = Monedas.Coins;
			Debug.Log ("CoinsGanadas" + CoinsGanadas);
			file.Close (); //Cierro el archivo
		} else {
			progresolvl = 0;
			Coins = 0;
			Debug.Log ("no se cargaron los datos");
			//besttime[x] = 0;
		}


	}

	[Serializable]
	class DatosAGuardar{
		

		public int progresolvl;
		public int[] besttimeguard = new int[100];
	



		public DatosAGuardar(int progresolvl){
			this.progresolvl = progresolvl;

	}



		public DatosAGuardar(int[] besttime){
			this.besttimeguard[x]= besttime[x];

		}
	


}

	[Serializable]
	class DatosdeMonedas{
		public int Coins;

		public DatosdeMonedas(int Coins){
			this.Coins = Coins;

		}

	}

	[Serializable]
	class DatosScore{
		public int totalScore;
		public int[] bestscoreguard = new int[100];

		public DatosScore(int totalScore){
			this.totalScore = totalScore;

		}


		public DatosScore(int[] bestscore){
			this.bestscoreguard[x]= bestscore[x];

		}


	}



}
