﻿using UnityEngine;
using System.Collections;

public class EventoDeCleave4 : MonoBehaviour {
	public bool activador = false;
	public int segundo = 0;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider Otro) {
		if (Otro.tag == "enemigo") {

			if (segundo == 0) {
				activador = true;
				Debug.Log ("El cubo colisiona? " + activador);

			} else {
				activador = false;
				Invoke ("reactivar",3);
			}
		}
	}

	public void reactivar()
	{
		segundo = 0;
	}
}