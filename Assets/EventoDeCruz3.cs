﻿using UnityEngine;
using System.Collections;

public class EventoDeCruz3 : MonoBehaviour {
	private SpriteRenderer imagen;
	public bool activador3 = false;
	// Use this for initialization
	void Start () {

		imagen = GetComponent<SpriteRenderer>();

	}

	// Update is called once per frame
	void Update () {

	}


	void OnTriggerEnter(Collider Otro) {
		if (Otro.tag == "Player") {

			imagen.enabled = true;
			activador3 = true;

		}
	}

}
