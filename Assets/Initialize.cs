﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialize : MonoBehaviour {
	
	public static Initialize Mantener;
	public static bool viene_de_pausa = false;
	public GameObject MenuPrincipal;
	public GameObject MenuLvles;

	void Awake(){

		if (Mantener == null) {
			Mantener = this;
			DontDestroyOnLoad (gameObject);  //NO DESTRUIR 
		} else if (Mantener != this) {
			Destroy(gameObject);
		}

		if (viene_de_pausa) {
		
			MenuPrincipal.SetActive (false);
			MenuLvles.SetActive (true);
		}


	}
	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
