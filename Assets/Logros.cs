﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logros : MonoBehaviour {


	private Image logro1;
	private Image logro2;
	private Image logro3;
	private Image logro4;

	public Sprite medallaBronce;
	public Sprite medallaPlata;
	public Sprite medallaOro;
	public Sprite medallaPlatino;



	private FileData GameData;



	// Use this for initialization
	void Start () {

		logro1 = GameObject.Find ("Logro1").GetComponent<Image> ();
		GameData = FileData.From ("LogrosData");



		if (GameData.Load ()) {

			if ((int)GameData.Get ("primeraVezLogro1") == 1) {
				logro1.sprite = medallaBronce;
				Debug.Log ("Cambio en logro 1");
			}
		} else {
			Debug.Log ("error al cargar archivos");
		}






	}


	// Update is called once per frame
	void Update () {

	}
}
