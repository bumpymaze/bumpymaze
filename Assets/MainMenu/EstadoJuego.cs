﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class EstadoJuego : MonoBehaviour {

	public static EstadoJuego estadoJuego;
	private string rutaArchivo;
	public int puntuacionMaxima = 0;
	public int EstadoLevels = 1;

	// Use this for initialization

	void Awake(){

		rutaArchivo = Application.persistentDataPath + "/datos.dat";
			if (estadoJuego == null) {
			estadoJuego = this;
			DontDestroyOnLoad (gameObject);

		} else if (estadoJuego != this) {
			Destroy(gameObject);
		}

	}

	void Start () {
		Cargar ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Guardar()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(rutaArchivo);

		DatosAguardar datos = new DatosAguardar (puntuacionMaxima);

		datos.puntuacionMaxima = puntuacionMaxima;


		bf.Serialize (file, datos);
	
		file.Close ();


	}

	void Cargar ()
	{
		if (File.Exists (rutaArchivo)) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (rutaArchivo, FileMode.Open);

			DatosAguardar datos = (DatosAguardar)bf.Deserialize (file);

			puntuacionMaxima = datos.puntuacionMaxima;


			file.Close ();
		} else {
			puntuacionMaxima = 0;
		}

	}



	[Serializable]
	class DatosAguardar{
		public int puntuacionMaxima;
		public int EstadoLevels;

		public DatosAguardar(int puntuacionMaxima)
		{
			this.puntuacionMaxima = puntuacionMaxima;

		}

	}


}
