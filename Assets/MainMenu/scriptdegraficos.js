﻿#pragma strict

public function GraficosVeryLow ()
{
	QualitySettings.currentLevel = QualityLevel.Fastest;

}

public function GraficosLow ()
{
	QualitySettings.currentLevel = QualityLevel.Fast;

}

public function GraficosMidle ()
{
	QualitySettings.currentLevel = QualityLevel.Simple;

}

public function GraficosMidHigh ()
{
	QualitySettings.currentLevel = QualityLevel.Good;

}

public function GraficosHigh ()
{
	QualitySettings.currentLevel = QualityLevel.Beautiful;

}

public function GraficosUltra ()
{
	QualitySettings.currentLevel = QualityLevel.Fantastic;

}