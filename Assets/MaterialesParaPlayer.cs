﻿using UnityEngine;
using System.Collections;

public class MaterialesParaPlayer : MonoBehaviour {
	public Material PlayerNegro;
	public Material PlayerGris;
	public Material PlayerBlanco;
	public Material PlayerRojoOscuro;
	public Material PlayerRojo;
	public Material PlayerRojoClaro;
	public Material PlayerAzulOscuro;
	public Material PlayerAzul;
	public Material PlayerCeleste;
	public Material PlayerVerdeOscruo;
	public Material PlayerVerdeClaro;
	public Material PlayerTurquesa;
	public Material PlayerAmarilloOscuro;
	public Material PlayerAmarilloClaro;
	public Material PlayerCremita;
	public Material PlayerMarron;
	public Material PlayerNaranja;
	public Material PlayerNaranjaClaro;
	public Material PlayerVioletOscuro;
	public Material PlayerVioletaClaro;
	public Material PlayerRosa;


	private Renderer render;
	private int roc;
	// Use this for initialization
	void Start () {
		render = GetComponent<Renderer>();

		Debug.Log (Config.ColorPlayer);

		switch (Config.ColorPlayer) {
		case 1:
			render.sharedMaterial = PlayerNegro;
			break;
		case 2:
			render.sharedMaterial = PlayerGris;
			break;
		case 3:
			render.sharedMaterial = PlayerBlanco;
			break;
		case 4:
			render.sharedMaterial = PlayerRojoOscuro;
			break;
		case 5:
			render.sharedMaterial = PlayerRojo;
			break;
		case 6:
			render.sharedMaterial = PlayerRojoClaro;
			break;
		case 7:
			render.sharedMaterial = PlayerAzulOscuro;
			break;
		case 8:
			render.sharedMaterial = PlayerAzul;
			break;
		case 9:
			render.sharedMaterial = PlayerCeleste;
			break;
		case 10:
			render.sharedMaterial = PlayerVerdeOscruo;
			break;
		case 11:
			render.sharedMaterial = PlayerVerdeClaro;
			break;
		case 12:
			render.sharedMaterial = PlayerTurquesa;
			break;
		case 13:
			render.sharedMaterial = PlayerAmarilloOscuro;
			break;
		case 14:
			render.sharedMaterial = PlayerAmarilloClaro;
			break;
		case 15:
			render.sharedMaterial = PlayerCremita;
			break;
		case 16:
			render.sharedMaterial = PlayerMarron;
			break;
		case 17:
			render.sharedMaterial = PlayerNaranja;
			break;
		case 18:
			render.sharedMaterial = PlayerNaranjaClaro;
			break;
		case 19:
			render.sharedMaterial = PlayerVioletOscuro;
			break;
		case 20:
			render.sharedMaterial = PlayerVioletaClaro;
			break;
		case 21:
			render.sharedMaterial = PlayerRosa;
			break;


		

		}


	}
	
	// Update is called once per frame
	void Update () {
	



	}




}
