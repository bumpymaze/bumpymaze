﻿using UnityEngine;
using System.Collections;

public class MovimientoMenus : MonoBehaviour {

	private Animator animacionPerfil; 
	private Animator animacionSkin;
	private Animator animacionShop;
	private Animator animacionRanking;
	private Animator animacionSettings;
	private Animator animacionHelp;

	private bool estadoPerfil = false;
	private bool estadoSkin = false;
	private bool estadoShop = false;
	private bool estadoRanking = false;
	private bool estadoSettings = false;
	private bool estadoHelp = false;


	private bool todosguardados = true;

	// Use this for initialization
	void Start () {

		animacionPerfil =  GameObject.Find ("Perfil").GetComponent<Animator> ();
		animacionSkin =  GameObject.Find ("Skin").GetComponent<Animator> ();
		animacionShop =  GameObject.Find ("Shop").GetComponent<Animator> ();
		animacionRanking =  GameObject.Find ("Ranking").GetComponent<Animator> ();
		animacionSettings =  GameObject.Find ("Settings").GetComponent<Animator> ();
		animacionHelp =  GameObject.Find ("Help").GetComponent<Animator> ();



		///en false se despliega
		/// en true se esconde
		/// 


	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void MoverPerfil()
	{
		if (todosguardados) {

			animacionPerfil.enabled = true;
			animacionPerfil.SetBool ("pulsoboton", false);
			todosguardados = false;
			estadoPerfil = true;
			Debug.Log ("perfil 1");
		} else if (estadoPerfil) {
			animacionPerfil.SetBool ("pulsoboton", true);
			estadoPerfil = false;
			todosguardados = true;
			Debug.Log ("perfil 2");
		}
		else{
			animacionSkin.SetBool ("pulsoboton", true);
			animacionRanking.SetBool ("pulsoboton", true);
			animacionSettings.SetBool ("pulsoboton", true);
			animacionHelp.SetBool ("pulsoboton", true);
			animacionShop.SetBool ("pulsoboton", true);

			 estadoSkin = false;
			 estadoShop = false;
			 estadoRanking = false;
			 estadoSettings = false;
			 estadoHelp = false;


			animacionPerfil.enabled = true;
			animacionPerfil.SetBool ("pulsoboton", false);
			estadoPerfil = true;
			Debug.Log ("perfil 3");

		}

}

	public void MoverSkin()
	{
		if (todosguardados) {

			animacionSkin.enabled = true;
			animacionSkin.SetBool ("pulsoboton", false);
			todosguardados = false;
			estadoSkin = true;
			Debug.Log ("skin 1");
		}else if (estadoSkin)
			{
			animacionSkin.SetBool ("pulsoboton", true);
			estadoSkin = false;
			todosguardados = true;
			Debug.Log ("skin 2");
			}
			else{
			animacionPerfil.SetBool ("pulsoboton", true);
			animacionShop.SetBool ("pulsoboton", true);
			animacionRanking.SetBool ("pulsoboton", true);
			animacionSettings.SetBool ("pulsoboton", true);
			animacionHelp.SetBool ("pulsoboton", true);

			estadoPerfil = false;
			estadoShop = false;
			estadoRanking = false;
			estadoSettings = false;
			estadoHelp = false;

			animacionSkin.enabled = true;
			animacionSkin.SetBool ("pulsoboton", false);
			estadoSkin = true;
			Debug.Log ("skin 3");
		}

	}

	public void MoverShop()
	{
		if (todosguardados) {

			animacionShop.enabled = true;
			animacionShop.SetBool ("pulsoboton", false);
			todosguardados = false;
			estadoShop = true;
		}else if (estadoShop)
		{
			animacionShop.SetBool ("pulsoboton", true);
			estadoShop = false;
			todosguardados = true;
		}else{
			animacionPerfil.SetBool ("pulsoboton", true);
			animacionSkin.SetBool ("pulsoboton", true);
			animacionRanking.SetBool ("pulsoboton", true);
			animacionSettings.SetBool ("pulsoboton", true);
			animacionHelp.SetBool ("pulsoboton", true);

			estadoPerfil = false;
			estadoSkin = false;
			estadoRanking = false;
			estadoSettings = false;
			estadoHelp = false;

			animacionShop.enabled = true;
			animacionShop.SetBool ("pulsoboton", false);
			estadoShop = true;

		}

	}


	public void MoverRanking()
	{
		if (todosguardados) {

			animacionRanking.enabled = true;
			animacionRanking.SetBool ("pulsoboton", false);
			todosguardados = false;
			estadoRanking = true;
		}else if (estadoRanking)
		{
			animacionRanking.SetBool ("pulsoboton", true);
			estadoRanking = false;
			todosguardados = true;
		}else{
			animacionPerfil.SetBool ("pulsoboton", true);
			animacionSkin.SetBool ("pulsoboton", true);
			animacionShop.SetBool ("pulsoboton", true);
			animacionSettings.SetBool ("pulsoboton", true);
			animacionHelp.SetBool ("pulsoboton", true);

			estadoPerfil = false;
			estadoSkin = false;
			estadoShop = false;
			estadoSettings = false;
			estadoHelp = false;

			animacionRanking.enabled = true;
			animacionRanking.SetBool ("pulsoboton", false);
			estadoRanking = true;

		}

	}

	public void MoverSettings()
	{
		if (todosguardados) {

			animacionSettings.enabled = true;
			animacionSettings.SetBool ("pulsoboton", false);
			todosguardados = false;
			estadoSettings = true;
		}else if (estadoSettings)
		{
			animacionSettings.SetBool ("pulsoboton", true);
			estadoSettings = false;
			todosguardados = true;
		}else{
			animacionPerfil.SetBool ("pulsoboton", true);
			animacionSkin.SetBool ("pulsoboton", true);
			animacionRanking.SetBool ("pulsoboton", true);
			animacionShop.SetBool ("pulsoboton", true);
			animacionHelp.SetBool ("pulsoboton", true);

			estadoPerfil = false;
			estadoSkin = false;
			estadoShop = false;
			estadoRanking = false;
			estadoHelp = false;

			animacionSettings.enabled = true;
			animacionSettings.SetBool ("pulsoboton", false);
			estadoSettings = true;

		}

	}

	public void MoverHelp()
	{
		if (todosguardados) {

			animacionHelp.enabled = true;
			animacionHelp.SetBool ("pulsoboton", false);
			todosguardados = false;
			estadoHelp = true;
		}else if (estadoHelp)
		{
			animacionHelp.SetBool ("pulsoboton", true);
			estadoHelp = false;
			todosguardados = true;
		}else{
			animacionPerfil.SetBool ("pulsoboton", true);
			animacionSkin.SetBool ("pulsoboton", true);
			animacionRanking.SetBool ("pulsoboton", true);
			animacionShop.SetBool ("pulsoboton", true);
			animacionSettings.SetBool ("pulsoboton", true);

			estadoPerfil = false;
			estadoSkin = false;
			estadoShop = false;
			estadoRanking = false;
			estadoSettings = false;
	
			animacionHelp.enabled = true;
			animacionHelp.SetBool ("pulsoboton", false);
			estadoHelp = true;

		}

	}

	public void Back()
	{

		animacionPerfil.SetBool ("pulsoboton", true);
		animacionSkin.SetBool ("pulsoboton", true);
		animacionRanking.SetBool ("pulsoboton", true);
		animacionShop.SetBool ("pulsoboton", true);
		animacionSettings.SetBool ("pulsoboton", true);
		animacionHelp.SetBool ("pulsoboton", true);

	}


}
