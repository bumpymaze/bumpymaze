﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class MovimientoWorlds : MonoBehaviour {

	private Animator AnimWorld1;
	private Animator AnimWorld2;
	private Button next;
	private Button back;
	private ColorBlock nextColor;


	private int numero =0;


	// Use this for initialization
	void Start () {
		AnimWorld1 =  GameObject.Find ("World1").GetComponent<Animator> ();
		AnimWorld2 =  GameObject.Find ("World2").GetComponent<Animator> ();
		next = GameObject.Find ("Next").GetComponent<Button> ();
		nextColor = GameObject.Find ("Next").GetComponent<Button> ().colors;


		back = GameObject.Find ("Back").GetComponent<Button> ();
		//back.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void siguiente()
	{

		AnimWorld1.enabled = true;
		AnimWorld1.SetInteger ("estado", 1);
		AnimWorld2.enabled = true;
		AnimWorld2.SetInteger ("estado", 1);

		////nextColor.normalColor = Color.gray;
		//next.colors = nextColor;
		//next.enabled = false;
		//back.enabled = true;



	}

	public void volver()
	{
	
		AnimWorld1.enabled = true;
		AnimWorld1.SetInteger ("estado", 2);

		AnimWorld2.enabled = true;
		AnimWorld2.SetInteger ("estado", 2);
	///	back.enabled = false;
///		next.enabled = true;
	}

}
