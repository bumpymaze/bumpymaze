﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Botoneralvls : MonoBehaviour {

	private NivelActual nivel;
	private MenuPausaPersonal sacarpausa;
	// Use this for initialization
	void Start () {
		nivel = FindObjectOfType<NivelActual> ();
		sacarpausa = FindObjectOfType<MenuPausaPersonal> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Cambiolvl(){
		switch (nivel.nivelactivo) {
		case 1:
			SceneManager.LoadScene ("2");
			break;

		case 2:
			SceneManager.LoadScene ("3");
			break;

		case 3:
			SceneManager.LoadScene ("4");
			break;

		case 4:
			SceneManager.LoadScene ("5");
			break;

		case 5:
			SceneManager.LoadScene ("6");
			break;

		case 6:
			SceneManager.LoadScene ("7");
			break;

		case 7:
			SceneManager.LoadScene ("8");
			break;

		case 8:
			SceneManager.LoadScene ("9");
			break;

		case 9:
			SceneManager.LoadScene ("10");
			break;

		case 10:
			SceneManager.LoadScene ("11");
			break;

		case 11:
			SceneManager.LoadScene ("12");
			break;

		case 12:
			SceneManager.LoadScene ("13");
			break;

		case 13:
			SceneManager.LoadScene ("14");
			break;

		case 14:
			SceneManager.LoadScene ("15");
			break;

		case 15:
			SceneManager.LoadScene ("16");
			break;
		}


	}

	public void Menu(){
		Debug.Log ("Volviendo al menu");
		sacarpausa.pausa = true;
		SceneManager.LoadScene ("Mainmenu");
		Initialize.viene_de_pausa = true;


	}

	public void Salir(){
		Application.Quit();
	}

	public void Retry()
	{
		switch (nivel.nivelactivo) {
		case 1:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("1");

			break;

		case 2:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("2");

			break;

		case 3:
			SceneManager.LoadScene ("3");
			sacarpausa.pausa = true;
			break;

		case 4:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("4");

			break;

		case 5:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("5");
		
			break;

		case 6:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("6");

			break;

		case 7:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("7");

			break;

		case 8:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("8");

			break;

		case 9:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("9");

			break;

		case 10:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("10");
		
			break;

		case 11:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("11");
			break;

		case 12:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("12");

			break;

		case 13:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("13");

			break;

		case 14:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("14");

			break;

		case 15:
			sacarpausa.pausa = true;
			SceneManager.LoadScene ("15");

			break;
		}

		
}
}
