﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinC : MonoBehaviour {

	private Canvas completelevel;
	private Image touchPad1;
	private Image touchPad2;
	private MeshRenderer player;
	private TimerLuz timerluz;
	private string mejortime;
	public Text mejort;
	private string mostrar;
	public Text mejorado;
	public float tmiltruncado;
	public string mostrarmil;
	private int mejortimeint;
	private int mejortimeseg;
	private int mejortimecent;
	private NivelActual nivelactual; 
	private int KeyCode = 0;
	private int _timemilisecsGoogle = 0;
	private FileData GameData;

	int primeraVezLogro1 = 0;
	int primeraVezLogro2 = 0;
	int primeraVezLogro3 = 0;
	int primeraVezLogro4 = 0;



    public int timeMilisecondsGoogle {            //para encriptar y que  no la puedan violar
        get {
            return _timemilisecsGoogle ^ KeyCode; 
        }

        set {
            KeyCode = Random.Range(0, int.MaxValue);
            _timemilisecsGoogle = value ^ KeyCode;
        }
    }





	// Use this for initialization
	void Start () {

		GameData = FileData.Create("LogrosData");


		completelevel = GameObject.Find ("LvlComplete").GetComponent<Canvas> ();
		touchPad1 = GameObject.Find ("Pad1").GetComponent<Image> ();
		touchPad2 = GameObject.Find ("Pad2").GetComponent<Image> ();
		player = GameObject.Find ("Player").GetComponent<MeshRenderer>();
		timerluz = FindObjectOfType<TimerLuz> ();
		nivelactual = FindObjectOfType<NivelActual> ();

		Estadolevels.x = nivelactual.nivelactivo;
		Debug.Log( "X"+Estadolevels.x);
	}

	// Update is called once per frame
	void Update () {

		if (timerluz.respuestatimerluz) {   /// esto se da cuando termino el lvl y el contador se detiene 
			Time.timeScale = 0;

			completelevel.enabled = true;
			player.enabled = false;
			touchPad1.enabled = false;
			touchPad2.enabled = false;
			mejortimeint = Estadolevels.besttime [Estadolevels.x];
			mejortimeseg = (mejortimeint / 100);
			mejortimecent = mejortimeint - (mejortimeseg * 100);

	    	Debug.Log ("tiempocomp" + timerluz.tiempocomp);

			Debug.Log ("tiempoMilisecs" + timerluz.tiempoMilisecs);

           _timemilisecsGoogle = timerluz.tiempoMilisecs;



            Estadolevels.timelvl [Estadolevels.x] = timerluz.tiempopredint;       //// cargo la variable q se encarga de la conversion int tiempo


			if (Estadolevels.timelvl [Estadolevels.x] > Estadolevels.besttime [Estadolevels.x]) { // si el tiempo del nivel es > al mejor tiempo entonces lo cargo como mejor tiempo
				mejorado.text = "New best time!!";
				mejort.text = "Record: " + timerluz.timer.text;

			} else {
				mejort.text = "Best time: " + mejortimeseg + ":" + mejortimecent;
			}


			 
			if (Estadolevels.progresolvl < (nivelactual.nivelactivo + 1)) {

				Estadolevels.CoinsGanadas += 5;

			}

        //Envio a google play la puntuacion osea el tiempo en miliseconds (timerluz.tiempoMilisecs)

			switch (nivelactual.nivelactivo) {
				
			case 1:
				Social.ReportScore(5, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				
				break;
			
			case 2:
				Social.ReportScore(6, "CgkIgqGxg_IeEAIQBw", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
			
				Debug.Log("CASO LVL 2");
				if (GameData.Load ()) {
					primeraVezLogro1 = (int)GameData.Get ("primeraVezLogro1");
					if (primeraVezLogro1 == 1) {
						Debug.Log("Ya tiene este logro");
					} else {
						GameData.Set("primeraVezLogro1",1);
						if(GameData.Save()) Debug.Log("Datos guardados con exito");
						else Debug.Log("Error al guardar datos");

						Social.ReportProgress("CgkIgqGxg_IeEAIQAg", 100.0f,(bool success) =>{Debug.Log (success ? "Reported progres sussecsfully" : "Failed to report progres");
						});
					}
				} else {
					Debug.Log("Error al cargar datos");
					GameData.Set("primeraVezLogro1",1);
					if(GameData.Save()) Debug.Log("Datos guardados con exito");
					else Debug.Log("Error al guardar datos");
					Social.ReportProgress("CgkIgqGxg_IeEAIQAg", 100.0f,(bool success) =>{Debug.Log (success ? "Reported progres sussecsfully" : "Failed to report progres");
					});
				}


				break;
			case 3:
				Social.ReportScore(8, "CgkIgqGxg_IeEAIQCA", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
			
				Social.ReportProgress("CgkIgqGxg_IeEAIQAw", 100.0f,(bool success) =>{Debug.Log (success ? "Reported progres sussecsfully" : "Failed to report progres");
				});




				break;
			case 4:
				Social.ReportScore(_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				break;
			case 5:
				Social.ReportScore (_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log (succes ? "Reported score sussecsfully" : "Failed to report score");
				});


				if (GameData.Load ()) {
					primeraVezLogro1 = (int)GameData.Get ("primeraVezLogro1");
					if (primeraVezLogro1 == 1) {
						Debug.Log("Ya tiene este logro");
					} else {
						GameData.Set("primeraVezLogro1",1);
						if(GameData.Save()) Debug.Log("Datos guardados con exito");
						else Debug.Log("Error al guardar datos");

						Social.ReportProgress("CgkIgqGxg_IeEAIQAg", 100.0f,(bool success) =>{Debug.Log (success ? "Reported progres sussecsfully" : "Failed to report progres");
						});
					}
				} else {
					Debug.Log("Error al cargar datos");
					GameData.Set("primeraVezLogro1",1);
					if(GameData.Save()) Debug.Log("Datos guardados con exito");
					else Debug.Log("Error al guardar datos");
					Social.ReportProgress("CgkIgqGxg_IeEAIQAg", 100.0f,(bool success) =>{Debug.Log (success ? "Reported progres sussecsfully" : "Failed to report progres");
					});
				}


				break;
			case 6:
				Social.ReportScore(_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				break;
			case 7:
				Social.ReportScore(_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				break;
			case 8:
				Social.ReportScore(_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				break;
			case 9:
				Social.ReportScore(_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				break;
			case 10:
				Social.ReportScore(_timemilisecsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
					Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
				break;
			default:
				Debug.Log ("sale por default");
				break;
			}

			Debug.Log ("GOOGLE MILISECS" +_timemilisecsGoogle);

			/*
			Social.ReportScore(timeMilisecondsGoogle, "CgkIgqGxg_IeEAIQBg", (bool succes) => {
				Debug.Log(succes ? "Reported score sussecsfully" : "Failed to report score"); });
*/
			Estadolevels.accelvl = nivelactual.nivelactivo + 1;

			timerluz.respuestatimerluz = false;
			//Debug.Log (timerluz.respuestatimerluz);
			//Debug.Log ("Ejecuto todo?");
			//Debug.Log( "X"+Estadolevels.x);
			//Debug.Log ("Progreso lvl" + Estadolevels.progresolvl);
			//Debug.Log ("acces lvl" + Estadolevels.accelvl);
			timerluz.estadoejecucion ++;
		} else { 
			//Debug.Log ("Deberia salir del bucle del update");
		}

	}

	void OnTriggerEnter(Collider Otro) {
		if (Otro.tag == "portal"){
			timerluz.terminolvl = true;
		}

	}


	[System.Serializable]
	public class SAVEDATACLASS
	{
		public int UnInt;
		public string UnString;
		public float UnFloat;
		public bool UnBool;
	}
}
