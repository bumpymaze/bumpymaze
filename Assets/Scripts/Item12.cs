﻿using UnityEngine;
using System.Collections;

public class Item12 : MonoBehaviour {
	private Animator ItemDestroy;
	private MeshRenderer item;

	// Use this for initialization
	void Start () 
	{
		ItemDestroy = GameObject.Find("Chizpas12").GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider collider)
	{
		//Debug.Log("Tocado");
		Destroy (gameObject);


		gameObject.tag.Replace("Item", "pungo");
		ItemDestroy.enabled = true;


	}
}
