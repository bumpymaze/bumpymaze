﻿using UnityEngine;
using System.Collections;

public class JumperModifTouch : MonoBehaviour {

	private jumperModif Player;

	// Use this for initialization
	void Start () {
		Player = FindObjectOfType<jumperModif> ();
	}

	public void LadoDerecho()
	{
		Player.Mover (1);
		Player.Rotar (1);
	}
	public void LadoIzquierdo()
	{
		Player.Mover (-1);
		Player.Rotar (-1);
	}
	public void SinPresionar()
	{
		Player.Mover (0);
		Player.Rotar (0);
	}

}
