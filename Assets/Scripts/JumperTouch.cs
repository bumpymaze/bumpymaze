﻿using UnityEngine;
using System.Collections;

public class JumperTouch : MonoBehaviour {

	private jumper Player;

	// Use this for initialization
	void Start () {
		Player = FindObjectOfType<jumper> ();
	}

	public void LadoDerecho()
	{
		Player.Mover (1);
		Player.Rotar (1);
	}
	public void LadoIzquierdo()
	{
		Player.Mover (-1);
		Player.Rotar (-1);
	}
	public void SinPresionar()
	{
		Player.Mover (0);
		Player.Rotar (0);
	}

}
