﻿using UnityEngine;
using System.Collections;

public class MantenerMusica : MonoBehaviour {

	public static AudioSource musica;
	public AudioClip music1;
	public AudioClip music2;
	public AudioClip music3;


	public static int nivel;

	public static bool estadoPausa;

	public static MantenerMusica keepmusic;
	// Use this for initialization

	void Awake()
	{

		if (keepmusic == null) {
			keepmusic = this;
			DontDestroyOnLoad (gameObject);
		} else if (keepmusic != this) {
			Destroy(gameObject);
		}
	}
	void Start () {





		musica = this.gameObject.GetComponent<AudioSource> ();
	

		musica.Play ();
	}


	// Update is called once per frame
	void Update () {


		if (estadoPausa) {
			musica.Pause ();

		} else {
			musica.UnPause ();

		}
		
		if (nivel > 0  && nivel < 5 && !musica.isPlaying   &&  !estadoPausa ) {
			musica.enabled = true;
				musica.clip = music1;
				musica.Play ();



		}

		if (nivel >= 5 && nivel < 10 && !musica.isPlaying &&  !estadoPausa ) {
			musica.enabled = true;
				musica.clip = music2;
				musica.Play ();

		}

		if (nivel >= 10 && !musica.isPlaying && !estadoPausa ) {
			musica.enabled = true;
			musica.clip = music3;
			musica.Play ();

		}



}
}