﻿using UnityEngine;
using System.Collections;

public class MenuPausaPersonal : MonoBehaviour {

	public bool pausa = false;
	public GameObject menupausa;
	public GameObject menusettings;
	//private AudioSource musica1;

	private jumper jumpermov;

	// Use this for initialization
	void Start () {
		menupausa = GameObject.Find ("Pause menu");
		menusettings = GameObject.Find ("Qualitysettings");
	    menupausa.SetActive(false);
		menusettings.SetActive (false);
		jumpermov = FindObjectOfType<jumper> ();
		MantenerMusica.estadoPausa = false;

		//musica1 = GameObject.Find ("Musicalevls").GetComponent<AudioSource> ();
		/*
		if (MantenerMusica.musica.isPlaying) {
		}
		else MantenerMusica.musica.Play ();
		
		MantenerMusica.musica.UnPause();
*/
		//menupausa = GameObject.Find ("Pause menu").GetComponent<Canvas> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("escape")) {

			Pausado ();
		}
	}

		//if (Input.GetKeyDown ("escape")) {
	public void Pausado() {
			//check if game is already paused		
			if(pausa == true){
				//unpause the game
				pausa = false;
				Time.timeScale = 1;
				//AudioListener.volume = 1;
			Debug.Log("Despauso el juego");
				//musica1.UnPause();
			MantenerMusica.estadoPausa = false;
				menupausa.SetActive(false);
				menusettings.SetActive (false);
				//Cursor.visible = false;	
				jumpermov.celumov = 0.18f;
				jumpermov.celurot = 5f;
				jumpermov.Velocidad = 0.06f;
				jumpermov.rotacion = 5f;

			}

			//else if game isn't paused, then pause it
			else if(pausa == false){
				pausa = true;
				//AudioListener.volume = 0;
			MantenerMusica.estadoPausa = true;
			//musica1.Pause();
				Time.timeScale = 0;
			Debug.Log("Pauso el juego");
				jumpermov.celumov = 0;
			    jumpermov.celurot = 0;
				jumpermov.Velocidad = 0;
				jumpermov.rotacion = 0;
			

				menupausa.SetActive(true);
				//Cursor.visible = true;
			}


		//}
	
	
}
}