﻿using UnityEngine;
using System.Collections;

public class Mimpulse : MonoBehaviour 
{
	public float Gravedad;
	public float Salto;
	public float velocidad;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void FixedUpdate ()
			{

		Physics.gravity = new Vector3(0, -Gravedad, 0);

		if (IsGrounded ()) { //SI ESTA EN EL PISO

			if (Input.GetKey (KeyCode.D)) {

				GetComponent<Rigidbody> ().AddForce (Vector3.right * velocidad, ForceMode.Impulse);
			}
			
			if (Input.GetKey (KeyCode.A)){ 
			
				GetComponent<Rigidbody> ().AddForce (Vector3.right * -velocidad, ForceMode.Impulse);
			
			}
			if (Input.GetKey (KeyCode.W)) {
			
				GetComponent<Rigidbody> ().velocity = new Vector3(0, Salto, 0);
			}

		}
		if (Input.GetKey (KeyCode.D)) {
			
			GetComponent<Rigidbody> ().AddForce (Vector3.right * velocidad, ForceMode.Impulse);
			
		}
		if (Input.GetKey (KeyCode.A)) {
			
			GetComponent<Rigidbody> ().AddForce (Vector3.right * -velocidad, ForceMode.Impulse);
			
		}



	}

	bool IsGrounded()//HACE QUE SI NO ESTA EN EL PISO LA GRAVEDAD LE DE PARA ABAJO
	{
		return Physics.Raycast(transform.position, -Vector3.up, 0.51f);
	}
}
