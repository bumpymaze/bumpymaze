﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour 
{
	public float deltaRotation = 30f;
	public float deltamov = 10f;


	public Color color;

	// Use this for initialization
	void Start () 
	{
		GetComponent<Renderer>().material.color = color;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Rotate ();
		movement ();
		
	}
	void Rotate()
	{
		if (Input.GetKey(KeyCode.RightArrow))
		transform.Rotate(new Vector3(0f, -deltaRotation, 0f) * Time.deltaTime);
		else if (Input.GetKey(KeyCode.LeftArrow))
			transform.Rotate(new Vector3(0f, deltaRotation, 0f) * Time.deltaTime);
		else if (Input.GetKey(KeyCode.UpArrow))
			transform.Rotate(new Vector3(deltaRotation, 0f, 0f) * Time.deltaTime);
		else if (Input.GetKey(KeyCode.DownArrow))
			transform.Rotate(new Vector3(-deltaRotation, 0f, 0f) * Time.deltaTime);

	}
void movement()
	{
		if(Input.GetKey(KeyCode.W))
		   transform.Translate(Vector3.forward * deltamov * Time.deltaTime);
		else if(Input.GetKey(KeyCode.S))
			transform.Translate(Vector3.back * deltamov * Time.deltaTime);
		else if(Input.GetKey(KeyCode.A))
			transform.Translate(Vector3.right * deltamov * Time.deltaTime);
		else if(Input.GetKey(KeyCode.D))
			transform.Translate(Vector3.left * deltamov * Time.deltaTime);
	}


}


