﻿using UnityEngine;
using System.Collections;

public class Movimiento : MonoBehaviour {



	Rigidbody Jugador;

	public float Velocidad;
	public float Gravedad;
	public float Salto;
	public float rotacion;

	void Start () 
	{
		Jugador = GetComponent<Rigidbody> ();
	}
	

	void Update () {

		Physics.gravity = new Vector3(0, -Gravedad, 0);
		if (IsGrounded ()) //SI ESTA EN EL PISO
		{
			if (Input.GetButton("Fire1"))
			{
				Jugador.velocity = new Vector3(0, Salto, 0);
			}
//
//			if (Input.GetButton("Fire1")& Input.GetKey (KeyCode.D))
//			{
//
//				Jugador.velocity = new Vector3(2.8f + Velocidad, Salto, 0);
//
//			}
//
//			if (Input.GetButton("Fire1")& Input.GetKey (KeyCode.A))
//			{
//				Jugador.velocity = new Vector3( - 2.8f - Velocidad, Salto, 0);
//
//			
//			}


			if (Input.GetKey (KeyCode.D))
				MovDerecha();

			if (Input.GetKey (KeyCode.A))
				MovIzquierda();


		}
		//SI NO ESTA EN EL PISO ME TIENE Q DAR BOLA IGUAL
		if (Input.GetKey (KeyCode.D))
			MovDerecha ();
		else if (Input.GetKey (KeyCode.A)) 
			MovIzquierda ();


	}


	bool IsGrounded()//HACE QUE SI NO ESTA EN EL PISO LA GRAVEDAD LE DE PARA ABAJO
	{
		return Physics.Raycast(transform.position, -Vector3.up, 0.51f);
	}



	public void MovDerecha()
	{
		GetComponent<Transform> ().position = new Vector3 (transform.position.x + Velocidad , transform.position.y, transform.position.z);
		transform.Rotate(Time.deltaTime + rotacion, 0 ,0);
	}

	public void MovIzquierda()
	{
		GetComponent<Transform> ().position = new Vector3 (transform.position.x - Velocidad, transform.position.y, transform.position.z);
		transform.Rotate(Time.deltaTime - rotacion, 0 ,0);

	}



}

