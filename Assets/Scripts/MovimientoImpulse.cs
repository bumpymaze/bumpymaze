﻿using UnityEngine;
using System.Collections;

public class MovimientoImpulse : MonoBehaviour {



	Rigidbody Jugador;

	public float Velocidad;
	public float Gravedad;
	public float Salto;
	public float rotacion;
	private float mvelocidad;
	private float msaltar;
	private float mrotar;
	private float ayudasalto;
	private float movi;

	void Start () 
	{
		Jugador = GetComponent<Rigidbody> ();
	}
	

	void Update () {

		Physics.gravity = new Vector3(0, -Gravedad, 0);
		if (IsGrounded ()) //SI ESTA EN EL PISO
		{

			GetComponent<Rigidbody> ().velocity = new Vector3(0, msaltar, 0);


				Moverse ();


		}
		//SI NO ESTA EN EL PISO ME TIENE Q DAR BOLA IGUAL
	
		GetComponent<Transform> ().position = new Vector3 (transform.position.x + 0.3f * movi , transform.position.y, transform.position.z);

	}


	bool IsGrounded()//HACE QUE SI NO ESTA EN EL PISO LA GRAVEDAD LE DE PARA ABAJO
	{
		return Physics.Raycast(transform.position, -Vector3.up, 0.6f);
	}



	public void Moverse()
	{

		GetComponent<Rigidbody>().AddForce(Vector3.right * mvelocidad, ForceMode.Impulse);
	}



//	}
	public void Mover(float moveInput)
	{
		mvelocidad = Velocidad * moveInput;
		movi = moveInput;
		//ayudasalto = 2.8f * moveInput;
	}
	public void Saltar(float saltInput)
	{
		msaltar = Salto * saltInput;
		//ayudasalto = 2.8f * saltInput;

	}

	public void Rotar(float rotarInput)
	{
		mrotar = rotacion * rotarInput;


	}



}

