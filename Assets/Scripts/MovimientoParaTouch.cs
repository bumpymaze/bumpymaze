﻿using UnityEngine;
using System.Collections;

public class MovimientoParaTouch : MonoBehaviour {



	Rigidbody Jugador;

	public float Velocidad;
	public float Gravedad;
	public float Salto;
	public float rotacion;
	private float mvelocidad;
	private float msaltar;
	private float mrotar;
	private float ayudasalto;
	private float ayuda;

	void Start () 
	{
		Jugador = GetComponent<Rigidbody> ();
	}
	

	void Update () {

		Physics.gravity = new Vector3(0, -Gravedad, 0);
		if (IsGrounded ()) //SI ESTA EN EL PISO
		{

			Jugador.velocity = new Vector3(mvelocidad, msaltar, 0);


				Moverse ();


		}
		//SI NO ESTA EN EL PISO ME TIENE Q DAR BOLA IGUAL
	
		GetComponent<Transform> ().position = new Vector3 (transform.position.x + mvelocidad + ayuda , transform.position.y, transform.position.z);
		transform.Rotate (Time.deltaTime + mrotar, 0, 0);

	}


	bool IsGrounded()//HACE QUE SI NO ESTA EN EL PISO LA GRAVEDAD LE DE PARA ABAJO
	{
		return Physics.Raycast(transform.position, -Vector3.up, 0.6f);
	}



	public void Moverse()
	{
		GetComponent<Transform> ().position = new Vector3 (transform.position.x + mvelocidad, transform.position.y, transform.position.z);
		transform.Rotate (Time.deltaTime + mrotar, 0, 0);
	}

//	public void MovDerecha()
//	{
//		GetComponent<Transform> ().position = new Vector3 (transform.position.x + mvelocidad , transform.position.y, transform.position.z);
//		transform.Rotate(Time.deltaTime + mrotar, 0 ,0);
//	}
//
//	public void MovIzquierda()
//	{
//		GetComponent<Transform> ().position = new Vector3 (transform.position.x + mvelocidad, transform.position.y, transform.position.z);
//		transform.Rotate(Time.deltaTime + mrotar, 0 ,0);

//	}
	public void Mover(float moveInput)
	{
		mvelocidad = Velocidad * moveInput;
		ayudasalto = 2.5f * moveInput;
		ayuda = 0.2f * moveInput;
	}
	public void Saltar(float saltInput)
	{
		msaltar = Salto * saltInput;
		//ayudasalto = 2.8f * saltInput;

	}

	public void Rotar(float rotarInput)
	{
		mrotar = rotacion * rotarInput;


	}



}

