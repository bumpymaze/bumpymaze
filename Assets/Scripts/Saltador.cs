﻿using UnityEngine;
using System.Collections;

public class Saltador : MonoBehaviour 
{
	Rigidbody Jugador;
	
	[SerializeField]
	float Gravedad, Velocidad, Salto, Rotacion;
	
	bool Ganaste;
	
	void Start () 
	{
		Ganaste = false;
		Jugador = GetComponent<Rigidbody>();
	}
	
	void Update () 
	{
		if (!Ganaste)
		{
		if (!Input.GetKey(KeyCode.Escape))
		{
			Physics.gravity = new Vector3(0, -Gravedad, 0);
			if(IsGrounded())
			{
				
				
					Jugador.velocity = new Vector3(0, Salto, 0);
				
					
				if (Input.GetKey (KeyCode.D))
							GetComponent<Transform> ().position = new Vector3 (transform.position.x + Velocidad, transform.position.y, transform.position.z);

					else if (Input.GetKey (KeyCode.A))					
						GetComponent<Transform> ().position = new Vector3 (transform.position.x - Velocidad, transform.position.y, transform.position.z);
					
			}
				 if (Input.GetKey (KeyCode.D))
				{
					GetComponent<Transform> ().position = new Vector3 (transform.position.x + Velocidad, transform.position.y, transform.position.z);
					GetComponent<Transform> ().Rotate(new Vector3(0f, Rotacion ,0f) * Time.deltaTime);
				}
				else if (Input.GetKey (KeyCode.A))
				{
					GetComponent<Transform> ().position = new Vector3 (transform.position.x - Velocidad, transform.position.y, transform.position.z);
					GetComponent<Transform> ().Rotate(new Vector3(0f, -Rotacion ,0f) * Time.deltaTime);
				}
			}
		}
	}
	
	bool IsGrounded()
	{
		return Physics.Raycast(transform.position, -Vector3.up, 0.51f);
	}
	
	void OnTriggerEnter(Collider obj)
	{
		if (obj.tag == "Meta")
		{
			Ganaste = true;
		}
	}
}
