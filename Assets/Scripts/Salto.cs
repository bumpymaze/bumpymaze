﻿using UnityEngine;
using System.Collections;

public class Salto : MonoBehaviour {

	Rigidbody Jugador;
	
	public float Velocidad;
	private float movelocidad;
	public float Gravedad;
	public float Saltar;
	bool tierra;
	
	void Start () 
	{
		Jugador = GetComponent<Rigidbody> ();
	}
	
	void OnCollisionEnter(Collision collision)
	{
		tierra=false;
		Physics.Raycast(transform.position, -Vector3.up, 0.51f);
	}


	
	void Update () {


		Physics.gravity = new Vector3(0, -Gravedad, 0);


		if (!tierra) 
		{


  		Jugador.velocity = new Vector3 (movelocidad, Saltar, 0);
		

		
		}


		
		Jugador.velocity = new Vector3 (movelocidad, Jugador.velocity.y, 0);	
	}
	

	void OnCollisionExit(Collision collision)
	{
		tierra = true;
	}
	
 public void Move(float moveInput)
	{
		movelocidad = Velocidad * moveInput;
		
	}
}