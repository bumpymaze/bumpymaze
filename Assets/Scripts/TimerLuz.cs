﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerLuz : MonoBehaviour {





	public float tiempopred;
	public Text timer;
	public Text timerpts;
//	public Text score;
	float puntitos;
	 public string tiempo;
	public float tiempomil;
	public int tiempopredint;
	string puntitosstr;
	 public string mili;
	bool a = false;
	private Animator Chauluz;
	private luzitem ayudaitem;
	int masluz;
	public float tiempocomp;
	private Light playerluz;
	//public Text bestime;
	string mejortime;
	public int segint;
	public int centint;
	public string prueba;
	private Light IntencidadLuz2;
	//private Light IntencidadLuz1;
	private Light IntencidadLuz;
	public Text Prueba2;
	public  bool terminolvl = false;
	public  bool respuestatimerluz = false;
	public int estadoejecucion = 0;

	/// /////////////////////////////////////////
	public int tiempoMilisecs;


	void Start () {

		if (Config.bonus5 > 0) {

			tiempopred += Config.bonus5 * 5;
			Config.bonus5 = 0;
		}

		if (Config.bonus10 > 0) {

			tiempopred += Config.bonus10 * 10;
			Config.bonus10 = 0;
		}

		Chauluz = GameObject.Find ("Luz").GetComponent<Animator> ();
		Chauluz.enabled = false;
		ayudaitem = FindObjectOfType<luzitem> ();
		playerluz = GameObject.Find ("LuzPlayer").GetComponent<Light> ();

		IntencidadLuz2  =	GameObject.Find("Directional light 2").GetComponent<Light>();
		//IntencidadLuz1  =	GameObject.Find("Directional light 1").GetComponent<Light>();
		IntencidadLuz  =	GameObject.Find("Directional light").GetComponent<Light>();
		estadoejecucion = 0;
	}


	void Update () {
		if (!terminolvl) {
			FunTimerluz ();


		} else if(estadoejecucion == 0){
			respuestatimerluz = true;
			Debug.Log ("responde el timer luz?");
			  
		}//else //Debug.Log ("salio del bucle de timer luz?");
		}

		public void FunTimerluz()
	{
		tiempopred -= Time.deltaTime;
		tiempocomp = tiempopred + ayudaitem.masitem;
		tiempoMilisecs = (int)Mathf.Round(tiempocomp * 1000);
		tiempomil =((tiempocomp*100)%100);

		/////////////// CAMBIO ////////////////
		tiempopredint =(int)(tiempocomp*100);
		segint = (tiempopredint / 100);
		centint = tiempopredint - (segint * 100);
		tiempo = segint.ToString ("0#");
		mili = centint.ToString ("0#");

		///////////////////////////////////////////

		//Prueba2.text = tiempo + ":" + mili;
		//prueba = tiempo  + ":" + mili;
		timer.text = tiempo  + ":" + mili;//tiempo del timer arrriba
	

		timerpts.text ="Time: " + tiempo  + ":" + mili; //tiempo mostrado en completelvl



		if (tiempocomp > 10)
		{
			Chauluz.Rebind();
					Chauluz.enabled = false;

					//Light IntencidadLuz2  =	GameObject.Find("Directional light 2").GetComponent<Light>();
					IntencidadLuz2.intensity = 1; 
					//Light IntencidadLuz1  =	GameObject.Find("Directional light 1").GetComponent<Light>();
				//	IntencidadLuz1.intensity = 1; 
					//Light IntencidadLuz  =	GameObject.Find("Directional light").GetComponent<Light>();
				    IntencidadLuz.intensity = 0.8f; 
					playerluz.renderMode = LightRenderMode.Auto;									
		}
		else if ((tiempocomp > 5) & (tiempocomp <= 10))
			{
			
			Chauluz.enabled = true;

			}

		else if ((tiempocomp <= 5) & (tiempocomp > 0))
		{
			
			playerluz.renderMode = LightRenderMode.ForcePixel;
					
			
		}
		else if (tiempocomp <= 0)
		{
			timer.text = "0:00";
			tiempopred = 0;
			tiempocomp = 0;
			ayudaitem.masitem = 0;

			//Debug.Log("tiempopred vale" + tiempopred + "el tiempo total vale" + tiempocomp);

			Chauluz.enabled = false;
			Chauluz.Rebind();
			//Light IntencidadLuz0  =	GameObject.Find("Directional light 2").GetComponent<Light>();
			IntencidadLuz2.intensity = 0; 
			//Light IntencidadLuz1  =	GameObject.Find("Directional light 1").GetComponent<Light>();
		//	IntencidadLuz1.intensity = 0;
			//Light IntencidadLuz  =	GameObject.Find("Directional light").GetComponent<Light>();
			IntencidadLuz.intensity = 0;
			
		}

		}
//	public void arrancador(bool moveInput){
//		a = true;
//	}
	
	// Update is called once per frame


}
