﻿using UnityEngine;
using System.Collections;

public class Touch : MonoBehaviour {

	private Movi2 Player;

	// Use this for initialization
	void Start () {
		Player = FindObjectOfType<Movi2> ();
	}

	public void LadoDerecho()
	{
		Player.Move (1);
	}
	public void LadoIzquierdo()
	{
		Player.Move (-1);
	}
	public void SinPresionar()
	{
		Player.Move (0);
	}
}
