﻿using UnityEngine;
using System.Collections;

public class TouchSalto : MonoBehaviour {

	private Movi4 Player;

	// Use this for initialization
	void Start () {
		Player = FindObjectOfType<Movi4> ();
	}

	public void LadoDerecho()
	{
		Player.Move (1);
	}
	public void LadoIzquierdo()
	{
		Player.Move (-1);
	}
	public void SinPresionar()
	{
		Player.Move (0);
	}
}
