﻿using UnityEngine;
using System.Collections;

public class Touchmove : MonoBehaviour {

	private MovimientoParaTouch Player;

	// Use this for initialization
	void Start () {
		Player = FindObjectOfType<MovimientoParaTouch> ();
	}

	public void LadoDerecho()
	{
		Player.Mover (1);
	}
	public void LadoIzquierdo()
	{
		Player.Mover (-1);
	}
	public void Sinmover()
	{
		Player.Mover (0);
	}

	public void RotarDer()
	{
		Player.Rotar (1);
	}
	public void RotarIzq()
	{
		Player.Rotar (-1);
	}
	public void SinRotar()
	{
		Player.Rotar (0);
	}
	public void Salto()
	{
		Player.Saltar (1);
	}
	public void SinSalto()
	{
		Player.Saltar (0);
	}

}
