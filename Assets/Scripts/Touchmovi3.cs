﻿using UnityEngine;
using System.Collections;

public class Touchmovi3 : MonoBehaviour {

	private Movi3 Player;

	// Use this for initialization
	void Start () {
		Player = FindObjectOfType<Movi3> ();
	}

	public void LadoDerecho()
	{
		Player.Move (1);
	}
	public void LadoIzquierdo()
	{
		Player.Move (-1);
	}
	public void SinPresionar()
	{
		Player.Move (0);
	}
	public void rotarDerecho()
	{
		Player.Rotar (1);
	}
	public void rotarIzquierdo()
	{
		Player.Rotar (-1);
	}
	public void Sinrotar()
	{
		Player.Rotar (0);
	}
}
