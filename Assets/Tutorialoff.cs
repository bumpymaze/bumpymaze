﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Tutorialoff : MonoBehaviour {
	private Canvas tuto;
	private AudioSource musica;

	// Use this for initialization
	void Start () {
		 tuto =  GameObject.Find ("Tutorial").GetComponent<Canvas> ();
		musica = GameObject.Find ("Musicalevls").GetComponent<AudioSource> ();

		if (Estadolevels.progresolvl >= 1) {

			tuto.enabled = false;
			Time.timeScale = 1;
			musica.enabled = true;


		}
	}
	
	// Update is called once per frame
	void Update () {



}
}
